tr(a).
tr(b).
tr(c).
tr(d).
tr(e).
pl(p0).
pl(p1).
pl(p2).
pl(p3).
pl(p4).
pl(p5).
arc(p0,a).
arc(a,p1).
arc(a,p2).
arc(p1,b).
arc(p1,d).
arc(p2,c).
arc(p2,d).
arc(b,p3).
arc(c,p4).
arc(d,p3).
arc(d,p4).
arc(p3,e).
arc(p4, e).
arc(e,p5).
m0([p0]).


%Predicado para encontrar posets
postset(Node,Result) :- findall(Pos,arc(Node,Pos),Result).
%Predicado para encontrar presets
preset(Node,Result) :- findall(Pre,arc(Pre,Node),Result).

%Predicado parancontrar si una transicion esta habilitada o no
isEnabled(Trans,M1) :- tr(Trans), preset(Trans,R), subset(R,M1).

%Predicado para realizar un fire en caso de que si se pueda avanzar
fire(Trans,Mark,NewM) :-
isEnabled(Trans,Mark), preset(Trans,R), subtract(Mark, R, Subs), postset(Trans,R2),  union(Subs,R2, NewM).

%Predicado para realizar un fire en caso de que no se pueda avanzar
fire(Trans,Mark,Mark).

%Predicado de enablement para saber cuales son las transiciones que se encuentran habilitadas con un cierto marcado
enablement(Trans,M1) :- findall(T,isEnabled(T,M1),Trans).


% Predicado check, utilizado por el replaying para revisar si las transiciones pueden ejecutarse.
check([],Mark) :- subtract(Mark,[p5],[]).
check([Hd|Tl],Mark) :-
    fire(Hd,Mark,NewM), check(Tl,NewM).
    
%Predicado replaying, que recibe una lista con transiciones y determina si pueden ejecutarse con el predicado check
replaying(List):- m0(I),check(List,I).